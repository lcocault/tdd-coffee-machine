/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** Beverage served by a coffee machine. */
const Beverage = {
  "CAPPUCCINO":1,
  "COCOA":2,
  "DOUBLE":3,
  "EXPRESSO":4
}
Object.freeze(Beverage)
const NumberOfBeverages = 4

/** Coin used by a coffee machine. */
const Coin = {
  "FIVE_CENTS":1,
  "TEN_CENTS":2,
  "TWENTY_CENTS":3,
  "FIFTY_CENTS":4,
  "ONE_EURO":5,
  "TWO_EUROS":6
}
Object.freeze(Coin)

/** Cappuccino facts. */
class Cappuccino {
    getPriceInCents() {
        return 65
    }
}

/** Cocoa facts. */
class Cocoa {
    getPriceInCents() {
        return 35
    }
}

/** Double facts. */
class Double {
    getPriceInCents() {
        return 40
    }
}

/** Expresso facts. */
class Expresso {
    getPriceInCents() {
        return 20
    }
}

//----------------------------------------------------------------------------//
//------------ IMPLEMENT YOUR OWN MACHINE AFTER THIS COMMENT -----------------//
//----------------------------------------------------------------------------//

/**
 * The CoffeeMachine class is the System Under Test for this example.
 */
class CoffeeMachine {
  /**
   * Constructor of the coffee machine.
   */
  constructor () {
      this.coinsInserted = []
      this.beverages = new Map()
      this.beverages.set(Beverage.CAPPUCCINO, new Cappuccino())
      this.beverages.set(Beverage.COCOA, new Cocoa())
      this.beverages.set(Beverage.DOUBLE, new Double())
      this.beverages.set(Beverage.EXPRESSO, new Expresso())
  }
  
  /**
   * Insert a coin in the coffee machine.
   * @param {Coin} coin Coin to insert
   */
  insert(coin) {
      this.coinsInserted.push(coin)
  }

  /**
   * Compute the amount of coins inserted in the coffee machine.
   * @return The total amount in cents
   */
  getCoinsAmount() {
      let amount = 0
      this.coinsInserted.forEach(function(coin) {
          switch (coin) {
          case Coin.ONE_EURO:
              amount += 100
              break
          case Coin.FIFTY_CENTS:
              amount += 50
              break
          case Coin.TWENTY_CENTS:
              amount += 20
              break
          case Coin.TEN_CENTS:
              amount += 10
              break
          case Coin.FIVE_CENTS:
          default:
              amount += 5
              break
          }
      })
      return amount
  }


  /**
   * Get the beverages that a user can order given the state of the machine
   * (credit, coffee/cocoa provision...)
   * @return Set of available beverages
   */
  getAvailableBeverages() {
      let beverages = []
      const amount = this.getCoinsAmount()
      for (var [key, value] of this.beverages) {
          if (amount >= value.getPriceInCents()) {
              beverages.push(key)              
          }
      }
      return beverages
  }
}

export { CoffeeMachine, Coin, Beverage, NumberOfBeverages }
