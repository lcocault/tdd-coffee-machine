/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.capgemini.csd.tdd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Default implementation for the coffee machine.
 * @author Laurent COCAULT
 */
public class DefaultCoffeeMachine implements CoffeeMachine {

    /** Beverages. */
    private Map<Beverage, BeverageFacts> beverages;

    /** Coins inserted. */
    private List<Coin> coinsInserted = new ArrayList<>();

    public DefaultCoffeeMachine() {
        beverages = new HashMap<>();
        beverages.put(Beverage.CAPPUCCINO, new Cappuccino());
        beverages.put(Beverage.COCOA, new Cocoa());
        beverages.put(Beverage.DOUBLE, new Double());
        beverages.put(Beverage.EXPRESSO, new Expresso());
    }

    /**
     * Compute the amount of coins inserted in the coffee machine.
     * @return The total amount in cents
     */
    private int getCoinsAmount() {
        int amount = 0;
        for (Coin coin : coinsInserted) {
            switch (coin) {
            case ONE_EURO:
                amount += 100;
                break;
            case FIFTY_CENTS:
                amount += 50;
                break;
            case TWENTY_CENTS:
                amount += 20;
                break;
            case TEN_CENTS:
                amount += 10;
                break;
            case FIVE_CENTS:
            default:
                amount += 5;
                break;
            }
        }
        return amount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Beverage> getAvailableBeverages() {
        Set<Beverage> available = new HashSet<>();
        int coinsAmount = getCoinsAmount();
        for (Beverage beverage : beverages.keySet()) {
            if (coinsAmount >= beverages.get(beverage).getPriceInCents()) {
                available.add(beverage);
            }
        }
        return available;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(Coin coin) {
        coinsInserted.add(coin);
    }

}
