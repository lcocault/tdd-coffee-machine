/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.capgemini.csd.tdd;

import java.util.Set;

/**
 * Public interface of the coffee machine.
 * @author Laurent COCAULT
 */
public interface CoffeeMachine {

    // -----------------//
    // Admin interface //
    // -----------------//

    // -----------------//
    // Users interface //
    // -----------------//

    /**
     * Get the beverages that a user can order given the state of the machine
     * (credit, coffee/cocoa provision...)
     * @return Set of available beverages
     */
    Set<Beverage> getAvailableBeverages();

    /**
     * Insert a coin in the coffee machine.
     * @param coin
     *            Coin inserted in the machine
     */
    void insert(Coin coin);
}
