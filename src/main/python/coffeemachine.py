#!/usr/bin/env python3
""" Coffee Machine implementation """
#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from enum import Enum


"""Beverage served by a coffee machine."""
class Beverage(Enum):
     CAPPUCCINO = 1
     COCOA = 2
     DOUBLE = 3
     EXPRESSO = 4


""""Coin used by a coffee machine."""
class Coin(Enum):
     FIVE_CENTS = 1
     TEN_CENTS = 2
     TWENTY_CENTS = 3
     FIFTY_CENTS = 4
     ONE_EURO = 5
     TWO_EUROS = 6


"""Cappuccino facts"""
class Cappuccino:
    def getPriceInCents(self):
        return 60

"""Cocoa facts"""
class Cocoa:
    def getPriceInCents(self):
        return 35

"""Double expresso facts"""
class Double:
    def getPriceInCents(self):
        return 40

"""Expresso facts"""
class Expresso:
    def getPriceInCents(self):
        return 20

#------------------------------------------------------------------------------#
#------------- IMPLEMENT YOUR OWN MACHINE AFTER THIS COMMENT ------------------#
#------------------------------------------------------------------------------#

class CoffeeMachine:
    """System under test to implement"""
    
    def __init__(self):
        """Constructor of the coffee machine."""
        self.coinsInserted = []
        self.beverages = {
            Beverage.CAPPUCCINO: Cappuccino(),
            Beverage.COCOA: Cocoa(),
            Beverage.DOUBLE: Double(),
            Beverage.EXPRESSO: Expresso()
        }


    def insert(self, coin):
        """Insert a coin in the coffee machine.
    
        Keyword arguments:
        coin -- Coin to insert
        """
        self.coinsInserted.append(coin)


    def getCoinsAmount(self):
        """Get the total amount of coins inserted in the machine
    
        Keyword arguments:
        return -- the total amount of coins in cents
        """
        amount = 0
        for coin in self.coinsInserted:
            if coin == Coin.ONE_EURO:
                amount = amount + 100
            elif coin == Coin.FIFTY_CENTS:
                amount = amount + 50
            elif coin == Coin.TWENTY_CENTS:
                amount = amount + 20
            elif coin == Coin.TEN_CENTS:
                amount = amount + 10
            elif coin == Coin.FIVE_CENTS:
                amount = amount + 5
        return amount


    def getAvailableBeverages(self):
        """Get the beverages that a user can order given the state of the
        machine (credit, coffee/cocoa provision...)
    
        Keyword arguments:
        return -- an array with the available beverages
        """
        beverages = []
        amount = self.getCoinsAmount()
        for beverage in self.beverages:
            if amount >= self.beverages[beverage].getPriceInCents():
                beverages.append(beverage)
        return beverages
