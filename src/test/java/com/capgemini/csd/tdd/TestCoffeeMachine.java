/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.capgemini.csd.tdd;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for the coffee machine.
 * @author Laurent COCAULT
 */
public class TestCoffeeMachine {

    /**
     * Get an installed and empty coffee machine.
     */
    private CoffeeMachine getInstalledCoffeeMachine() {
        return new DefaultCoffeeMachine();
    }

    /**
     * Get an installed and filled-up coffee machine.
     */
    private CoffeeMachine getFilledUpCoffeeMachine() {
        // TODO Implement the fill-up
        return new DefaultCoffeeMachine();
    }

    @Test
    public void getAvailableBeverages_Empty_Installed() {
        // GIVEN an installed machine
        CoffeeMachine coffeeMachine = getInstalledCoffeeMachine();
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN the result is empty
        Assert.assertTrue(beverages.isEmpty());
    }

    @Test
    public void getAvailableBeverages_Empty_FilledUp() {
        // GIVEN an installed and filled-up machine
        CoffeeMachine coffeeMachine = getFilledUpCoffeeMachine();
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN the result is empty
        Assert.assertTrue(beverages.isEmpty());
    }

    @Test
    public void getAvailableBeverages_All_FilledUpAndOneEuro() {
        // GIVEN an installed and filled-up machine, with a credit of 1 euro
        CoffeeMachine coffeeMachine = getFilledUpCoffeeMachine();
        coffeeMachine.insert(Coin.ONE_EURO);
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN all the existing beverages are returned
        Assert.assertEquals(Beverage.values().length, beverages.size());
    }

    @Test
    public void getAvailableBeverages_NoCappuccino_FilledUpAndFiftyCents() {
        // GIVEN an installed and filled-up machine, with a credit of 50 cents
        CoffeeMachine coffeeMachine = getFilledUpCoffeeMachine();
        coffeeMachine.insert(Coin.FIFTY_CENTS);
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN all the existing beverages are returned except the cappuccino
        Assert.assertEquals(Beverage.values().length - 1, beverages.size());
        Assert.assertFalse(beverages.contains(Beverage.CAPPUCCINO));
    }

    @Test
    public void getAvailableBeverages_OnlyExpresso_FilledUpAndTwentyCents() {
        // GIVEN an installed and filled-up machine, with a credit of 20 cents
        CoffeeMachine coffeeMachine = getFilledUpCoffeeMachine();
        coffeeMachine.insert(Coin.TWENTY_CENTS);
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN only the expresso is returned
        Assert.assertEquals(1, beverages.size());
        Assert.assertTrue(beverages.contains(Beverage.EXPRESSO));
    }

    @Test
    public void getAvailableBeverages_ExpressoCocoa_FilledUpAndThirtyFiveCents() {
        // GIVEN an installed and filled-up machine, with a credit made of
        // three coins: 5 cents, 10 cents and 20 cents
        CoffeeMachine coffeeMachine = getFilledUpCoffeeMachine();
        coffeeMachine.insert(Coin.FIVE_CENTS);
        coffeeMachine.insert(Coin.TEN_CENTS);
        coffeeMachine.insert(Coin.TWENTY_CENTS);
        // WHEN we query the list of available beverages
        Set<Beverage> beverages = coffeeMachine.getAvailableBeverages();
        // THEN expresso and cocoa are returned
        Assert.assertEquals(2, beverages.size());
        Assert.assertTrue(beverages.contains(Beverage.EXPRESSO));
        Assert.assertTrue(beverages.contains(Beverage.COCOA));
    }

    // ---------------------------------------------------------------------- //
    // ----------- IMPLEMENT YOUR OWN TESTS AFTER THIS COMMENT -------------- //
    // ---------------------------------------------------------------------- //

}
