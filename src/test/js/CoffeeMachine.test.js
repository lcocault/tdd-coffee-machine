/* Copyright 2019 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CoffeeMachine, Coin, Beverage, NumberOfBeverages } from '../../main/js/CoffeeMachine'

// Get an installed and empty coffee machine.
function getInstalledMachine() {
    return new CoffeeMachine()
}

// Get an installed and filled-up coffee machine.
function getFilledUpMachine() {
    // TODO Fill the machine up
    return new CoffeeMachine()
}

test('getAvailableBeverages_Empty_InitialState', () => {
    // GIVEN an installed machine
    const coffeeMachine = getInstalledMachine()
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN the result is empty
    expect(beverages.length).toBe(0)
})

test('getAvailableBeverages_Empty_FilledUp', () => {
    // GIVEN a filled-up machine
    const coffeeMachine = getFilledUpMachine()
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN the result is empty
    expect(beverages.length).toBe(0)
})

test('getAvailableBeverages_Empty_FilledUpAndOneEuro', () => {
    // GIVEN a filled-up machine, with a credit of 1 euro
    const coffeeMachine = getFilledUpMachine()
    coffeeMachine.insert(Coin.ONE_EURO);
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN all the existing beverages are returned
    expect(beverages.length).toBe(NumberOfBeverages)
})

test('getAvailableBeverages_Empty_FilledUpAndFiftyCents', () => {
    // GIVEN a filled-up machine, with a credit of 50 cents
    const coffeeMachine = getFilledUpMachine()
    coffeeMachine.insert(Coin.FIFTY_CENTS);
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN all the existing beverages are returned except the cappuccino
    expect(beverages.length).toBe(NumberOfBeverages-1)
    expect(beverages.includes(Beverage.CAPPUCCINO)).toBe(false)
})

test('getAvailableBeverages_Empty_FilledUpAndTwentyCents', () => {
    // GIVEN a filled-up machine, with a credit of 20 cents
    const coffeeMachine = getFilledUpMachine()
    coffeeMachine.insert(Coin.TWENTY_CENTS);
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN only the expresso is returned
    expect(beverages.length).toBe(1)
    expect(beverages.includes(Beverage.EXPRESSO)).toBe(true)
})

test('getAvailableBeverages_ExpressoCocoa_FilledUpAndThirtyFiveCents', () => {
    // GIVEN a filled-up machine, with a credit of 35 cents (three coins of
    // respectively twenty cents, ten cents and five cents)
    const coffeeMachine = getFilledUpMachine()
    coffeeMachine.insert(Coin.TWENTY_CENTS);
    coffeeMachine.insert(Coin.TEN_CENTS);
    coffeeMachine.insert(Coin.FIVE_CENTS);
    // WHEN we query the list of available beverages
    const beverages = coffeeMachine.getAvailableBeverages();
    // THEN the expresso and the cocoa are returned
    expect(beverages.length).toBe(2)
    expect(beverages.includes(Beverage.COCOA)).toBe(true)
    expect(beverages.includes(Beverage.EXPRESSO)).toBe(true)
})

//----------------------------------------------------------------------------//
//------------- IMPLEMENT YOUR OWN TESTS AFTER THIS COMMENT ------------------//
//----------------------------------------------------------------------------//
