#!/usr/bin/env python3
""" Test the Coffee Machine """

#    Copyright 2019 Laurent COCAULT
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from unittest import TestCase
from coffeemachine import Beverage
from coffeemachine import CoffeeMachine
from coffeemachine import Coin


class TestCoffeeMachine(TestCase):
    
    # Initialize an installed and empty coffee machine.
    def initInstalledMachine(self):
        self.coffeeMachine = CoffeeMachine()

    # Initialize an installed and filled-up coffee machine.
    def initFilledUpMachine(self):
        # TODO Fill the machine up
        self.coffeeMachine = CoffeeMachine()

    def test_getAvailableBeverages_Empty_Installed(self):
        # GIVEN an installed machine
        self.initFilledUpMachine()
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN the result is empty
        self.assertEqual(len(beverages), 0)

    def test_getAvailableBeverages_Empty_FilledUp(self):
        # GIVEN a filled-up machine
        self.initFilledUpMachine()
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN the result is empty
        self.assertEqual(len(beverages), 0)

    def test_getAvailableBeverages_All_FilledUpAndOneEuro(self):
        # GIVEN an installed and filled-up machine, with a credit of 1 euro
        self.initFilledUpMachine()
        self.coffeeMachine.insert(Coin.ONE_EURO)
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN all the existing beverages are returned
        self.assertEqual(len(beverages), len(Beverage))

    def test_getAvailableBeverages_All_FilledUpAndFiftyCents(self):
        # GIVEN an installed and filled-up machine, with a credit of 50 cents
        self.initFilledUpMachine()
        self.coffeeMachine.insert(Coin.FIFTY_CENTS)
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN all the existing beverages are returned except the cappuccino
        self.assertEqual(len(beverages), len(Beverage)-1)
        self.assertFalse(Beverage.CAPPUCCINO in beverages)

    def test_getAvailableBeverages_All_FilledUpAndTwentyCents(self):
        # GIVEN an installed and filled-up machine, with a credit of 20 cents
        self.initFilledUpMachine()
        self.coffeeMachine.insert(Coin.TWENTY_CENTS)
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN only the expresso is returned
        self.assertEqual(len(beverages), 1)
        self.assertTrue(Beverage.EXPRESSO in beverages)

    def test_getAvailableBeverages_ExpressoCocoa_FilledUpAndThirtyFiveCents(self):
        # GIVEN an installed and filled-up machine, with a credit of 35 cents
        # (twenty cents, ten cents and five cents)
        self.initFilledUpMachine()
        self.coffeeMachine.insert(Coin.TWENTY_CENTS)
        self.coffeeMachine.insert(Coin.TEN_CENTS)
        self.coffeeMachine.insert(Coin.FIVE_CENTS)
        # WHEN we query the list of available beverages
        beverages = self.coffeeMachine.getAvailableBeverages()
        # THEN only the expresso is returned
        self.assertEqual(len(beverages), 2)
        self.assertTrue(Beverage.COCOA in beverages)
        self.assertTrue(Beverage.EXPRESSO in beverages)

#------------------------------------------------------------------------------#
#-------------- IMPLEMENT YOUR OWN TESTS AFTER THIS COMMENT -------------------#
#------------------------------------------------------------------------------#

