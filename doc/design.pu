@startuml

enum Beverage {
  CAPPUCCINO,
  COCOA,
  DOUBLE,
  EXPRESSO
}

enum Coin {
  FIVE_CENTS,
  TEN_CENTS,
  TWENTY_CENTS,
  FIFTY_CENTS,
  ONE_EURO,
  TWO_EUROS
}

interface CreditProvider {
	+ void credit(int toAccountId, int amountInCents)
	+ int getCreditInCents(int accountId)
	+ void pay(int fromAccountId, int amountInCents) throws InsufficientCreditException
}

interface CoffeeMachine {
  === Admin interface ==
  + void addChange(List<Coin>)
  + List<Coin> getMoney()
  + void provideCocoa(int quantity)
  + void provideCoffee(int quantity)
  + void setCreditProvider(CreditProvider)
  === User interface ==
  + void applyNfcTag(int tagId)
  + void cancel()
  + void chooseBeverage(Beverage) throws InsufficientCreditException
  + void disposeNfcTag()
  + List<Beverage> getAvailableBeverages()
  + List<Coin> getChange()
  + void insert(Coin coin)
  + Beverage takeBeverage() throws NoBeverageException
}
CoffeeMachine ..> Beverage
CoffeeMachine ..> Coin
CoffeeMachine ..> CreditProvider

hide empty members
@enduml
